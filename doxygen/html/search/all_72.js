var searchData=
[
  ['range_5fa',['range_a',['../de/d72/structSet__interpolacja.html#a6f5155d19551d738011b73a3ab831e45',1,'Set_interpolacja']]],
  ['rap',['RAP',['../dd/d90/classGMainWin.html#a52176b2531295df87c8ca0e4b1926ee3ae348ce52005ca0117f40574d1573accd',1,'GMainWin']]],
  ['raport',['RAPORT',['../dd/d90/classGMainWin.html#a2ff605add3bed95d9b65eabedc8b98f7ab163d58d4346106890c11ecf636d8ad3',1,'GMainWin']]],
  ['raport_5fadd',['raport_add',['../d6/d62/classGeoModel.html#a403b0502cd9eb67cab5222712d98f42e',1,'GeoModel']]],
  ['raport_5fclear',['raport_clear',['../d6/d62/classGeoModel.html#ac34a21d70ee82c114627d94ac46b2149',1,'GeoModel']]],
  ['raport_5fget',['raport_get',['../d6/d62/classGeoModel.html#a33f0f33b7784a645dd984617cab163a5',1,'GeoModel']]],
  ['readme_2emd',['README.md',['../da/ddd/README_8md.html',1,'']]],
  ['recalc',['recalc',['../d6/d41/classGeoVariogram.html#a02f263e1ccfb6325f7b1e2bb489ada93',1,'GeoVariogram::recalc(wektor3d ustawienie)'],['../d6/d41/classGeoVariogram.html#aee8843838f32e0e55884417a923cee19',1,'GeoVariogram::recalc()']]],
  ['recreate_5frapor',['recreate_rapor',['../d6/d62/classGeoModel.html#a28f46facd64638fb6b2fa8732eb1e0e2',1,'GeoModel']]],
  ['reset_5fmin_5fmax',['reset_min_max',['../d0/df3/classGeoCube.html#a5c3e33368d05fd54b709fc150faf7408',1,'GeoCube']]],
  ['resetmodel',['resetModel',['../d6/d62/classGeoModel.html#aa3c3a312b2066beffd1e1a17ec03ce03',1,'GeoModel']]],
  ['resizeevent',['resizeEvent',['../d5/d12/classGraphColumnWidget.html#a19bd14a3d5d8c9139d2b96043905c840',1,'GraphColumnWidget']]],
  ['resizegl',['resizeGL',['../d4/dc5/classGLWidget.html#ac0d2a8ecf60907a81c0d73475d851025',1,'GLWidget']]],
  ['rotx',['rotX',['../d4/dc5/classGLWidget.html#af6dc49cb4ad0e862c2d76f4df66548cc',1,'GLWidget']]],
  ['roty',['rotY',['../d4/dc5/classGLWidget.html#a4fcaf7d94fb3d1ff4e30da68557ab6b6',1,'GLWidget']]],
  ['rotz',['rotZ',['../d4/dc5/classGLWidget.html#af0842856ffa0c60dbd795076573ea072',1,'GLWidget']]]
];
